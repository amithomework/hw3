#include <stdio.h>
#include <iostream>
#include <string>
#include "sqlite3.h"

using namespace std;

int main()
{
	int rc;
	sqlite3* db;
	string update;
	char *zErrMsg = 0;

	rc = sqlite3_open("FirstPart.db", &db);
	if (rc)
	{
		
		sqlite3_close(db);
		return 1;
	}

	rc = sqlite3_exec(db, "create table pepole(id integer primary key autoincrement, name text);", NULL, 0, &zErrMsg);
	if (rc)
	{
		system("pause");
		sqlite3_close(db);
		return 1;
	}

	rc = sqlite3_exec(db, "insert into pepole (name) values('lola');", NULL, 0, &zErrMsg);
	if (rc)
	{
		sqlite3_close(db);
		return 1;
	}

	rc = sqlite3_exec(db, "insert into pepole (name) values('tola');", NULL, 0, &zErrMsg);
	if (rc)
	{
		sqlite3_close(db);
		return 1;
	}

	rc = sqlite3_exec(db, "insert into pepole (name) values('mola');", NULL, 0, &zErrMsg);
	if (rc)
	{
		sqlite3_close(db);
		return 1;
	}
	
	update = "update pepole set name='dola' where id=";
	update += to_string(sqlite3_last_insert_rowid(db));
	update += ';';
	rc = sqlite3_exec(db, update.c_str(), NULL, 0, &zErrMsg);
	if (rc)
	{
		sqlite3_close(db);
		return 1;
	}

	sqlite3_close(db);
	return 0;
}
