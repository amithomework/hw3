#include <iostream>
#include <unordered_map>
#include <vector>
#include <string>
#include <sstream>
#include "sqlite3.h"

using namespace std;

unordered_map<string, vector<string>> results;
int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}

bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg)
{
	stringstream sql;
	int rc, fromRcv;
	
	sql << "select balance from accounts where id=" << from << ';';
	sqlite3_exec(db, sql.str().c_str(), callback, 0, &zErrMsg);
	
	if (results.empty())
		return false;
	
	fromRcv = stoi(results.find("balance")->second[0]);
	results.clear();
	
	if (fromRcv < amount)
		return false;
	
	sql << "select balance from accounts where id=" << to << ';';	//check if the id is existing
	sqlite3_exec(db, sql.str().c_str(), callback, 0, &zErrMsg);

	if (results.empty())
		return false;
	
	results.clear();

	sqlite3_exec(db, "begin transaction;", NULL, NULL, NULL);

	sql << "update accounts set balance=balance-"<< amount <<" where id=" << from << ';';
	rc = sqlite3_exec(db, sql.str().c_str(), NULL, 0, &zErrMsg);
	if (rc) return false;

	sql << "update accounts set balance=balance+" << amount << " where id=" << to << ';';
	rc = sqlite3_exec(db, sql.str().c_str(), NULL, 0, &zErrMsg);
	if (rc) return false;

	rc = sqlite3_exec(db, "commit;", NULL, NULL, NULL);
	if (rc) return false;
	return true;
}

bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg)
{
	stringstream select;
	int rc;
	int carPrice;

	select << "select available, price from cars where id=" << carid << ';';
	sqlite3_exec(db, select.str().c_str(), callback, 0, &zErrMsg);
	
	if (results.empty())
		return false;

	if (results.find("available")->second[0] != "1")
	{
		results.clear();
		return false;
	}

	carPrice = stoi(results.find("price")->second[0]);
	select.str(string());
	
	results.clear();

	select << "select balance from accounts where Buyer_id=" << buyerid << ';';
	sqlite3_exec(db, select.str().c_str(), callback, 0, &zErrMsg);

	if (results.empty())
		return false;

	if (stoi(results.find("balance")->second[0]) < carPrice)
	{
		results.clear();
		return false;
	}

	results.clear();
	select.str(string());

	select << "update cars set available=0 where id=" << carid << ';';

	sqlite3_exec(db, "begin transaction;", NULL, NULL, NULL);
	sqlite3_exec(db, select.str().c_str(), NULL, NULL, NULL);
	
	select.str(string());

	select << "update accounts set balance=balance-"<< carPrice <<" where Buyer_id=" << buyerid << ';';
	sqlite3_exec(db, select.str().c_str(), NULL, NULL, NULL);

	rc = sqlite3_exec(db, "commit;", NULL, NULL, NULL);
	if (rc) return false;
	return true;
}


void whoCanBuy(int carId, sqlite3* db, char* zErrMsg)
{
	stringstream sql;
	int carPrice;
	vector<string> c;

	sql << "select price from cars where id=" << carId << ';';
	sqlite3_exec(db, sql.str().c_str(), callback, 0, &zErrMsg);

	if (results.empty)
	{
		cout << "car id not found" << endl;
		return;
	}

	carPrice = stoi(results.find("price")->second[0]);
	results.clear();
	
	sql.str(string());
	sql << "select Buyer_id from accounts where balance>=" << carPrice << ';';
	sqlite3_exec(db, sql.str().c_str(), callback, 0, &zErrMsg);


	sql.str(string());
	sql << "select * from buyers where id=" ;

	c = results.find("Buyer_id")->second;
	int cars = c.size();
	for (int i = 0; i < cars; i++)
	{
		sql << c[i];
		if (i != cars - 1)
			sql << " or id=";
	}
	sql << ';';
	
	results.clear();
	sqlite3_exec(db, sql.str().c_str(), callback, 0, &zErrMsg);

}

int main()
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;

	rc = sqlite3_open("carsDealer.db", &db); 
	if (rc)
	{
		sqlite3_close(db);
		return 1;
	}
	if (!carPurchase(12, 10, db, zErrMsg))cout << "1 fail" << endl;
	if (!carPurchase(7, 10, db, zErrMsg))cout << "2 fail" << endl;
	if (!carPurchase(5, 20, db, zErrMsg))cout << "3 fail" << endl;
	
	if (!balanceTransfer(12, 5, 10000, db, zErrMsg))cout << "fail to send money" << endl;

	system("pause");
	return 0;
}
